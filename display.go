package main

import (
	"machine"
	"tinygo.org/x/drivers/hd44780"
)

type LineDisplayer func() string

type Display struct {
	lines       []LineDisplayer
	currentLine int
	lcd         hd44780.Device
}

func NewDisplay() (*Display, error) {
	lcd, err := hd44780.NewGPIO4Bit(
		[]machine.Pin{machine.PB5, machine.PB4, machine.PB10, machine.PA8},
		machine.PC7,
		machine.PA9,
		machine.NoPin,
	)
	if err != nil {
		return nil, err
	}

	err = lcd.Configure(hd44780.Config{
		Width:       16,
		Height:      2,
		CursorOnOff: true,
		CursorBlink: false,
	})

	if err != nil {
		return nil, err
	}

	display := Display{lcd: lcd}

	display.lcd.SetCursor(0, 0)
	display.lcd.Write([]byte("Initializing..."))
	display.lcd.Display()

	return &display, nil
}

func (display *Display) Render() {
	if len(display.lines) == 0 {
		return
	}
	display.lcd.SetCursor(0, 0)
	display.lcd.Write([]byte(display.lines[display.currentLine]()))
	display.lcd.Display()
}

func (display *Display) AddLine(handler LineDisplayer) {
	display.lines = append(display.lines, handler)
}

func (display *Display) NextLine() {
	var newLine int
	if display.currentLine+1 > len(display.lines)-1 {
		newLine = 0
	} else {
		newLine = display.currentLine + 1
	}
	//If we are switching line handlers, clear the screen
	if newLine != display.currentLine {
		display.currentLine = newLine
		display.ClearScreen()
	}
}

func (display *Display) PreviousLine() {
	var newLine int
	if display.currentLine-1 < 0 {
		newLine = len(display.lines) - 1
	} else {
		newLine = display.currentLine - 1
	}
	if newLine != display.currentLine {
		display.currentLine = newLine
		display.ClearScreen()
	}
}

// This is a hack since clearing the screen using the built in method seems to hang, maybe from writing too fast but this is more reliable either way
func (display *Display) ClearScreen() {
	display.lcd.SetCursor(0, 0)
	display.lcd.Write([]byte(FormatLine("", 16*2)))
	display.lcd.Display()
}
