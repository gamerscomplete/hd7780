package main

import (
	"machine"
)

const (
	KeyNone Key = iota
	KeySelect
	KeyUp
	KeyDown
	KeyLeft
	KeyRight
	KeyUnknown
)

type Key uint8

func (key Key) String() string {
	switch key {
	case KeyNone:
		return "None"
	case KeySelect:
		return "Select"
	case KeyUp:
		return "Up"
	case KeyDown:
		return "Down"
	case KeyLeft:
		return "Left"
	case KeyRight:
		return "Right"
	}
	return "Unknown"
}

type InputManager struct {
	adc machine.ADC
}

func NewInputManager(adc machine.ADC) *InputManager {
	return &InputManager{adc: adc}
}

// This is a real shit way of doing things, the problem seems to stem from feeding 5v logic into a 3.3v adc, but this is consistent enough
func (inputManager *InputManager) GetKey() Key {
	val := inputManager.adc.Get()
	var key Key
	switch val / 1000 {
	case 59:
		key = KeyNone
	case 65:
		key = KeySelect
	case 19:
		key = KeyUp
	case 37:
		key = KeyDown
	case 0:
		key = KeyRight
	case 57:
		fallthrough
	case 58:
		key = KeyLeft
	default:
		key = KeyUnknown
	}
	return key
}
