package main

import (
	"machine"
	"strconv"
	"time"
	// "tinygo.org/x/drivers/hd44780"
)

const (
	ROW_LENGTH = 16
)

func main() {
	machine.InitADC()
	led := machine.LED
	led.Configure(machine.PinConfig{Mode: machine.PinOutput})

	adc := machine.ADC{machine.PA0}
	adc.Configure(machine.ADCConfig{})

	display, err := NewDisplay()

	if err != nil {
		led.High()
	}

	var key Key
	var lastKey Key

	display.AddLine(func() string {
		outputString := FormatLine("Key: "+key.String(), ROW_LENGTH)
		val := adc.Get()
		outputString = outputString + FormatLine("R: "+strconv.Itoa(int(val))+" V: "+strconv.FormatFloat(float64(val)/65536*3.3, 'f', -1, 64), ROW_LENGTH)
		return outputString
	})

	display.AddLine(func() string {
		return "Dummy line 1"
	})

	display.AddLine(func() string {
		return "Dummy line 2"
	})

	inputManager := NewInputManager(adc)

	for {
		key = inputManager.GetKey()
		if key != lastKey {
			if key == KeyUp {
				display.NextLine()
			} else if key == KeyDown {
				display.PreviousLine()
			}
			lastKey = key
		}
		display.Render()
		time.Sleep(50 * time.Millisecond)
	}
}

func FormatLine(input string, length int) string {
	return Pad(Clamp(input, length), length)
}

func Clamp(input string, length int) string {
	inputLength := len(input)
	if inputLength > length {
		return input[0 : length-1]
	}
	return input
}

func Pad(input string, length int) string {
	inputLength := len(input)
	if inputLength < length {
		for i := 0; i < length-inputLength; i++ {
			input = input + " "
		}
	}
	return input
}
